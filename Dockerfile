FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="william.binitie@flutterwavego.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
# 755 means owner has full permmission but everyone else can only read and execute 
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
# The chown command is used to change the ownership of a file or directory. The nginx:nginx argument specifies the user and group that 
# the file should be owned by. The first nginx specifies the user, and the second nginx specifies the group.

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
# makes file executable 

USER nginx

CMD ["/entrypoint.sh"]
