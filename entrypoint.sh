#!/bin/sh 
# shell script to be executed
set -e
# set -e is a shell command used in Bash scripts to immediately exit the script 
# if any command within it exits with a non-zero exit code (i.e., if an error occurs).

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# envsubst is a Linux command-line utility that substitutes the values of environment variables in a text file. 

# start nginx
nginx -g 'daemon off;'
